const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19", "root", "adnaadna",
{host:'localhost', 
dialect:"mysql",
 logging:false,

pool: {
    max: 5,
    min: 0,
    acqiure: 30000,
    idle: 10000
},
define: {
    timestamps: false
}
});
sequelize.authenticate().then(()=>{
    console.log('OK');
}).catch(err =>{
    console.error('greska', err);
})
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

//importovanje modela
db.osobljeModel = sequelize.import(__dirname + '/osobljeModel.js');
db.terminModel = sequelize.import(__dirname + '/terminModel.js');
db.salaModel = sequelize.import(__dirname + '/salaModel.js');
db.rezervacijaModel = sequelize.import(__dirname + '/rezervacijaModel.js');

//relacije među tabelama
//db.osobljeModel.hasMany (db.rezervacijaModel, {as: 'osoblje_rezervacija'});
db.osobljeModel.hasMany(db.rezervacijaModel, {as: "osoblje", foreignKey: "osobljeId"});
db.rezervacijaModel.belongsTo(db.osobljeModel);

db.rezervacijaModel.belongsTo(db.terminModel);

//db.rezrvacijaModel.hasOne(db.terminModel, {as: 'rezervacija_termin'});
db.salaModel.hasMany(db.rezervacijaModel, {as: 'sala_rezervacija'});
db.rezervacijaModel.belongsTo(db.salaModel);

//db.salaModel.hasOne(db.osobljeModel, {as: 'sala_osoblje'});
//db.salaModel.belongsTo(db.osobljeModel);
db.salaModel.belongsTo(db.osobljeModel, {as: "osoblje", foreignKey: "zaduzenaOsoba"});

module.exports = db;
