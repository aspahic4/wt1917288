const express = require("express");
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();

const db = require('./db.js');
/*db.sequelize.sync({force:true}).then(function(){

});*/
//PUNJENJE BAZE
db.sequelize.sync({force:true}).then(function(){
    
    inicializacija().then(function(){
        //console.log("Baza kreirana!");
       
    }).catch(function(err){
        //console.log('greska');
    })

});

app.use(express.json());
app.use(express.static('./'));
var profePoSalama = [];

function inicializacija(){
    var osobljeLista = [];
    var rezervacijeLista = [];
    var terminiLista = [];
    var saleLista = [];
    
    return new Promise(function(resolve, reject){
        osobljeLista.push(db.osobljeModel.create({ime: 'Neko', prezime: 'Nekic', uloga: 'profesor'}));
        osobljeLista.push(db.osobljeModel.create({ime: 'Drugi', prezime: 'Neko', uloga: 'asistent'}));
        osobljeLista.push(db.osobljeModel.create({ime: 'Test', prezime: 'Test', uloga: 'asistent'}));
        
        terminiLista.push(db.terminModel.create({redovni: false, dan: null, datum: "01.01.2020", semestar: null, pocetak: "12:00", kraj: "13:00"}));
        terminiLista.push(db.terminModel.create({redovni: true, dan: 0, datum: null, semestar: "zimski", pocetak: "13:00", kraj: "14:00"}));
        
        Promise.all(osobljeLista).then(function(osoblje){
            var neko = osoblje.filter(function(a){return a.ime === 'Neko'})[0].dataValues.id;
            var drugi = osoblje.filter(function(a){return a.ime === 'Drugi'})[0].dataValues.id;
            var test = osoblje.filter(function(a){return a.ime === 'Test'})[0].dataValues.id;

            Promise.all(terminiLista).then(function(termini){
                var termin1 = termini.filter(function(t){return t.id == 1})[0].dataValues.id;
                var termin2 = termini.filter(function(t){return t.id == 2})[0].dataValues.id;

                saleLista.push(
                    db.salaModel.create({naziv:'1-11'}).then(function(s){
                        s.setOsoblje([neko]);
                        return new Promise(function(resolve, reject){resolve(s);});
                    })
                );
                saleLista.push(
                    db.salaModel.create({naziv: '1-15'}).then(function(s){
                        s.setOsoblje([drugi]);
                        return new Promise(function(resolve,reject){resolve(s);});
                    })
                );
                Promise.all(saleLista).then(function(sale){
                    var sala1 = sale.filter(function(s){return s.naziv == '1-11'})[0].dataValues.id;
                    var sala2 = sale.filter(function(s){return s.naziv == '1-15'})[0].dataValues.id;

                    rezervacijeLista.push(
                        db.rezervacijaModel.create({}).then(function(r){
                            r.setOsoblje([neko]);
                            r.setTermin([termin1]);
                            r.setSala([sala1]);
                            return new Promise(function(resolve,reject){resolve(r);});
                        })
                    );
                    rezervacijeLista.push(
                        db.rezervacijaModel.create({}).then(function(r){
                                r.setOsoblje([test]);
                                r.setTermin([termin2]);
                                r.setSala([sala1]);
                                return new Promise(function(resolve,reject){resolve(r);});
                            })
                    );
                    Promise.all(rezervacijeLista).then(function(k){resolve(k);}).catch(function(err){console.log("GRESKA"+err);});
                })
            })
        })
        //console.log('test');
    })
}



app.get('/osoblje', function(req, res){
        db.osobljeModel.findAll().then(function(podaci){
            res.send(JSON.stringify(podaci));
        })
        
});

app.get('/', function(req, res){
    res.sendFile('pocetna.html', opt = { root: __dirname });
});
app.get('/zauzeca', function(req, res){
   /* res.sendFile('zauzeca.json', opt = { root: __dirname });*/
   var per;
   var van;
   var rezrv;
   var sala;
   var zauzecaPer = [];
   var zauzecaVan = [];
    db.rezervacijaModel.findAll().then(function(rez){
       // for (var c= 0; c < rez.length; c++){
            db.terminModel.findAll({where: {redovni: true, }}).then(function(p){
               // per = p;
                
                db.terminModel.findAll({where: {redovni: false}}).then(function(v){
                     //van = v;

                     db.salaModel.findAll().then(function(s){
                         //
                         for(var r= 0 ; r<rez.length; r++){
                             rezrv = rez[r];
                             var objekat = {};
                             //prolazi kroz sve periodicne termine iz baze
                             for (var tp= 0; tp<p.length; tp++){
                                // per = p[tp];

                                 //pronadje termin koji odgovara rezervaciji
                                 if(rezrv.terminId == p[tp].id){
                                    {
                                        //prolazi kroz sve sale
                                         for(var sp = 0; sp<s.length; sp++){
                                             //ako pronadje salu sa istim id kao sto je u rezervaciji
                                             if(rezrv.salaId == s[sp].id){
                                                 objekat.sala = s[sp].naziv;
                                                 objekat.dan = p[tp].dan;
                                                 objekat.datum = p[tp].datum;
                                                 objekat.semestar = p[tp].semestar;
                                                 objekat.pocetak = String(p[tp].pocetak).slice(0,5);
                                                 objekat.kraj = String(p[tp].kraj).slice(0,5);
                                                 zauzecaPer.push(objekat);
                                                 
                                            }
                                         }
                                    }
                                 }
                                 
                             }
                             //prolazi kroz sve vanredne termine iz baze
                             for (var tp= 0; tp<v.length; tp++){
                                //per = v[tp];
                                //pronadje termin koji odgovara rezervaciji
                                if(rezrv.terminId == v[tp].id){
                                   {
                                       //prolazi kroz sve sale
                                        for(var sp = 0; sp<s.length; sp++){
                                            //ako pronadje salu sa istim id kao sto je u rezervaciji
                                            if(rezrv.salaId == s[sp].id){
                                              
                                                objekat.sala = s[sp].naziv;
                                                objekat.dan = v[tp].dan;
                                                objekat.datum = v[tp].datum;
                                                objekat.semestar = v[tp].semestar;
                                                objekat.pocetak = String(v[tp].pocetak).slice(0,5);
                                                objekat.kraj = String(v[tp].kraj).slice(0,5);
                                                zauzecaVan.push(objekat);
                                                
                                           }
                                        }
                                   }
                                }
                                
                            }   
                         }
                        // console.log(zauzecaPer);
                        res.send(JSON.stringify({periodicna: zauzecaPer, vanredna: zauzecaVan}));
                        

                     })
         
                     
                 }) 
            })
        //}
        
    })
   
});
app.get('/osobe', function(req, res){
    var osobljePoSalama = [];
    var profesori = [];
    var profUSali;
    var salaProfesorova;
    var check;
    db.osobljeModel.findAll().then(function(sviProfesori){
        db.rezervacijaModel.findAll().then(function(sveRezervacije){
            db.terminModel.findAll().then(function(termini){
                db.salaModel.findAll().then(function(sale){
                    for(var ci = 0; ci< sviProfesori.length; ci++){
                        check= 0;
                        var obj = {};
                        var imeProfesora = sviProfesori[ci].ime+ ' ' +sviProfesori[ci].prezime;
                        profesori.push(imeProfesora);
                        for(var cj = 0; cj< sveRezervacije.length; cj++){
                            if(sviProfesori[ci].id == sveRezervacije[cj].osobljeId){
                                var idSale = sveRezervacije[cj].salaId;
                                //console.log(idSale+ ' ' + sveRezervacije);
                                var trenutniDatum = new Date();
                                var h= trenutniDatum.getHours();
                                if(h<10) h = '0' + h;
                                var m = trenutniDatum.getMinutes();
                                if(m<10) m = '0' + m;
                                
                                var danUSedmici= trenutniDatum.getDay();
                                if(danUSedmici== 0) danUSedmici = 6;
                                else danUSedmici--;

                                for(var brojac = 0; brojac<termini.length; brojac++){
                                    if(termini[brojac].id == sveRezervacije[cj].terminId){
                                        if(termini[brojac].redovni == true){
                                            for(var brojacSale = 0; brojacSale<sale.length; brojacSale++){
                                            var sem = termini[brojac].semestar;   
                                                if((sem === 'zimski' && (trenutniDatum.getMonth() == 0 || trenutniDatum.getMonth>=9)) || (sem === 'ljetni' && (trenutniDatum.getMonth()>=1 || trenutniDatum.getMonth()<=6))){
                                                    if(termini[brojac].pocetak<= h+':'+m && termini[brojac].kraj>=h+':'+m){
                                                        
                                                            if(sveRezervacije[cj].salaId == sale[brojacSale].id){
                                                                obj.ime = imeProfesora;
                                                                obj.sala = sale[brojacSale].naziv;
                                                                osobljePoSalama.push(obj);    
                                                                 check ++;
                                                               // console.log(termini[brojac].pocetak + ' ' + h+':'+m);
                                                            }
                                                    }
                                                }
                                            }
                                        } 
                                        else{
                                            if(String(termini[brojac].datum == trenutniDatum.getDate()) + '.' + String(trenutniDatum.getMonth()+1) + '.' + String(trenutniDatum.getFullYear())){
                                                if(termini[brojac].pocetak<= h+':'+m && termini[brojac].kraj>=h+':'+m){
                                                    for(var brojacSale = 0; brojacSale<sale.length; brojacSale++){
                                                        if(sveRezervacije[cj].salaId == sale[brojacSale].id){
                                                           obj.ime = imeProfesora;
                                                            obj.sala = sale[brojacSale].naziv;
                                                            osobljePoSalama.push(obj);
                                                            console.log(obj.sala);
                                                            check++;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }  
                        console.log(check);
                        if(check == 0){
                            obj.ime = imeProfesora;
                            obj.sala = "kancelarija";
                            osobljePoSalama.push(obj);
                            
                        }
                    }  
                    /*var brojOsobaPoSalama = osobljePoSalama.length;
                    for(var y=0; y<profesori.length; y++){
            
                        for(var x = 0; x<brojOsobaPoSalama; x++){
                            
                            if(osobljePoSalama[x].ime ===  profesori[y]){
                                check = 'da';
                                //console.log(check);
                            } 
                            
                        }
                        if(check != 'da'){
                            var o = {};
                            o.ime = profesori[y].ime;
                            o.sala = "kancelarija";
                            osobljePoSalama.push(o);
                        } 
                    }*/
            

        res.send(JSON.stringify({"sviProfesori" : profesori, "zauzetiProfesori": osobljePoSalama}));  
                }) 
            })
        })
    })
});
app.get('/rezervacije', function(req, res){
    opt = { root: __dirname };
    
    res.sendFile("rezervacija.html", opt);
});
//vanredna zauzeca
app.post('/unesiZauzeca', function(req, res){
    let tijelo = req.body;
    //{"datum": datumZauzeca, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "ime": ime, "prezime": prezime, "uloga": uloga, "periodicna": periodicna }
    db.osobljeModel.findOrCreate({where:{ime : tijelo.ime, prezime: tijelo.prezime, uloga: tijelo.uloga}}).then(function(osoba){
        osoba = osoba[0];
        db.salaModel.findOrCreate({where:{naziv: tijelo.naziv, zaduzenaOsoba: osoba.id}}).then(function(sala){
            sala = sala[0];
            db.terminModel.findOrCreate({where:{datum: tijelo.datum, pocetak: tijelo.pocetak, kraj: tijelo.kraj, redovni: tijelo.periodicna}}).then(function(termin){
                termin = termin[0];
                db.rezervacijaModel.findOrCreate({where: {osobljeId: osoba.id, terminId:termin.id, salaId:sala.id}}).then(function(rezervacija){
                    console.log("Dodano");
                })
            })
        })
    });
    //let noviObj = "\n" + tijelo['dan']+ "\n" + tijelo['pocetak'] + "\n" + tijelo['kraj'] + "\n" + tijelo['naziv'] + "\n" + tijelo['predavac'];
    /*let noviObj = '{"dan":"' + tijelo['dan'] + '",' +
                    '"pocetak":"' + tijelo['pocetak'] + '",' + 
                    '"kraj":"' + tijelo['kraj'] + '",' + 
                    '"naziv":' + tijelo['naziv'] + '",'+
                    '"predavac":""' +
                    '}';*/
     /*       
    var data = fs.readFileSync('zauzeca.json');
    var obj = JSON.parse(data);

    obj.vanredna.push(tijelo);
    data = JSON.stringify(obj, null, 2);
    fs.writeFile('zauzeca.json', data, done);
    function done(){
        //console.log("DONE");
    }*/
    //console.log(tijelo);
    
});
//periodicna zauzeca
app.post('/unesiZauzecaPeriodicna', function(req, res){
    let tijelo = req.body;   
    //console.log(tijelo);
    db.osobljeModel.findOrCreate({where:{ime : tijelo.ime, prezime: tijelo.prezime, uloga: tijelo.uloga}}).then(function(osoba){
        osoba = osoba[0];
        db.salaModel.findOrCreate({where:{naziv: tijelo.naziv, zaduzenaOsoba: osoba.id}}).then(function(sala){
            sala = sala[0];
            db.terminModel.findOrCreate({where:{dan:tijelo.dan, semestar: tijelo.semestar, pocetak: tijelo.pocetak, kraj: tijelo.kraj, redovni: tijelo.periodicna}}).then(function(termin){
                termin = termin[0];
                db.rezervacijaModel.findOrCreate({where: {osobljeId: osoba.id, terminId:termin.id, salaId:sala.id}}).then(function(rezervacija){
                    console.log("Dodano");
                })
            })
        })
    });
    /*var data1 = fs.readFileSync('zauzeca.json');
    var obj1 = JSON.parse(data1);

    obj1.periodicna.push(tijelo1);
    data1 = JSON.stringify(obj1, null, 2);
    fs.writeFile('zauzeca.json', data1, done);
    function done(){
        //console.log("DONE");
    }  */  
});

app.get('/slike', function(req,res){

    var slike1 = {"slike" : []};

    var naziv;
    var naziv1;
    var obj2;
    fs.readdir(__dirname + '/slike', function (err, files){
        if(err) throw err;
        files.forEach(function (file){
            slike1.slike.push({"url": file}); 
        })
        
        obj2 = JSON.stringify(slike1, null, 2); 
        
        fs.writeFile('slike.json', obj2, done);
        function done(){
        }
    })
    
    
});
app.get('/slikeZaPrikaz', function(req,res){
    res.sendFile('slike.json', opt = { root: __dirname });
});
app.get('/slike/:naziv', function(req,res){
    //if(req.body == naziv){
       // res.sendFile(req.body, opt = { root: __dirname});
       res.sendFile(__dirname + '/slike' + req.params + naziv);
    //}
});

app.listen(8080);