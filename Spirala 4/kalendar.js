let Kalendar = (function(){

    var redovnaZauzeca = [];
    var vanrednaZauzeca = [];
    var datum;
    var prvi;
    var prviDan;
    var zadnji;
    var zadnjiDan;
    var sedmice;
    var daniBrojac;
    var dan;
    var i;
    var j;
    var k;
    var redovnaZauzeca;
    var vanrednaZauzeca;
    var kalendar = document.getElementsByClassName("velikiKalendar");
    var danUSedmici;
    var semestar;
    var daniZauzeca;
    var daniZauzecaLi;
    var pocetak;
    var kraj;
    var periodicna;
    var datum;
    var danUMjesecu;
    var mjesecUGodini;
    var daniZauzecaBroj;
    var sala;
    var prefarbaj;

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj){
    //implementacija ide ovdje
    
    
   // alert("TESTESTE" + mjesec + sala + pocetak + kraj);
    prefarbaj = document.getElementsByClassName("slobodna");
    
    for(i=0; i<prefarbaj.length; i++){
        prefarbaj[i].style.backgroundColor = "green";   
    }
     
    periodicna = document.getElementById("periodicna");

    sedmice= kalendarRef[0].getElementsByTagName("ul");

    //za vanredna zaduzenja
    for(i=0; i<vanrednaZauzeca.length; i++){
        
        //po sali se ne moze filtrirati jer nema u  objektima zauzeca podatka koja je sala zauzeta
        datum = vanrednaZauzeca[i].datum;
        danUMjesecu = datum[0] + datum[1];
        mjesecUGodini = datum[3] + datum[4];
        this.pocetak = vanrednaZauzeca[i].pocetak;
        this.kraj = vanrednaZauzeca[i].kraj;

        //console.log(pocetak+ ' '+ kraj+ mjesecUGodini + danUMjesecu);
        //prolazi kroz sedmice
        for(j=1; j<7; j++){
            //div-ovi koji pokazuje da je slobodan termin
            daniZauzeca = sedmice[j].getElementsByClassName("slobodna");
            //divo-ovi koji nose vrijednost dana u mjesecu 
            daniZauzecaBroj = sedmice[j].getElementsByClassName("broj");  
            //prolazi kroz dane u sedmici
            for (k=0; k<7; ++k){
                if( Number(daniZauzecaBroj[k].innerHTML) === Number(danUMjesecu) && mjesec === (Number(mjesecUGodini)-1) && vanrednaZauzeca[i].sala === sala){
                   
                    if(pocetak != '' && kraj != '') {
                        if((pocetak<= this.pocetak && this.pocetak<= kraj) || (pocetak<=this.kraj && this.kraj<=kraj) || (pocetak>=this.pocetak && kraj<=this.kraj)){
                            daniZauzeca[k].style.backgroundColor = "red";
                        }
                    } 
                    else if (pocetak === '' && kraj != ''){
                        if(this.pocetak<=kraj && kraj<=this.kraj){
                            daniZauzeca[k].style.backgroundColor = "red";
                        }
                    }
                    else if(pocetak != '' && kraj === ''){
                        if(this.pocetak<= pocetak && pocetak< this.kraj){
                            daniZauzeca[k].style.backgroundColor = "red";
                        }
                    }
                    else;
                }  
            }
        }
    }
    //za redovna zaduzenja

    //prolazi kroz niz redovnih zauzeca
    for(i=0; i<redovnaZauzeca.length; i++){
        //po sali se ne moze filtrirati jer nema u  objektima zauzeca podatka koja je sala zauzeta
        semestar = redovnaZauzeca[i].semestar;
        danUSedmici = redovnaZauzeca[i].dan;
        this.pocetak = redovnaZauzeca[i].pocetak;
        this.kraj = redovnaZauzeca[i].kraj;
        //prolazi kroz sedmice
        for(j=1; j<7; j++){
            //div-ovi koji pokazuje da je slobodan termin 
            daniZauzeca = sedmice[j].getElementsByClassName("slobodna");   
            //prolazi kroz dane u sedmici
            for (k=0; k<7; ++k){
                if( k=== danUSedmici && redovnaZauzeca[i].sala === sala && ((semestar==="zimski" && (mjesec>=9 || mjesec===0)) || (semestar==="ljetni" && (mjesec>0 && mjesec<6)))){
                    if(pocetak != '' && kraj != '') {
                        if((pocetak<= this.pocetak && this.pocetak<= kraj) || (pocetak<=this.kraj && this.kraj<=kraj)){
                            if(daniZauzeca[k].style.display !="none")
                                daniZauzeca[k].style.backgroundColor = "red";
                        }
                    } 
                    else if (pocetak === '' && kraj != ''){
                        if(this.pocetak<=kraj && kraj<=this.kraj){
                            if(daniZauzeca[k].style.display !="none")
                                daniZauzeca[k].style.backgroundColor = "red";
                        }
                    }
                    else if(pocetak != '' && kraj === ''){
                        if(this.pocetak<= pocetak && pocetak< this.kraj){
                           if(daniZauzeca[k].style.display !="none")
                            daniZauzeca[k].style.backgroundColor = "red";
                        }
                    }
                    else;
                }      
            }
        }
    }
    
}
    
function ucitajPodatkeImpl(redovna, vanredna){  
    redovnaZauzeca = redovna;
    vanrednaZauzeca = vanredna;
    //alert(redovnaZauzeca[0].sala+ 'DONEDONE');

}
    
function iscrtajKalendarImpl(kalendarRef, mjesec){
        datum = new Date(), godina = datum.getFullYear();
        prvi = new Date(godina, mjesec, 1);
    
        prviDan = prvi.getDay();
        //posto .getDay daje broj dan kao 0 nedjelja 6 subota podesavamo da je 0 ponedjeljak a 6 nedjelja
        if(prviDan=== 0){
        prviDan = 6;
        }
        else{
            prviDan--;
        }
        zadnji = new Date(godina, mjesec + 1, 0);
        zadnjiDan = zadnji.getDate();
        sedmice= kalendarRef[0].getElementsByTagName("ul");
        daniBrojac= 1;
        for(i=1; i<7; i++){
            //div element koji nosi broj dana u mjesecu
            dan = sedmice[i].getElementsByClassName("broj");
            //div element koji oznacava da li je zauzeta sala ili ne
            daniZauzece = sedmice[i].getElementsByClassName("slobodna");
            //li element klase dani unutar kojeg su divovi slobodna i broj
            daniZauzeceLi = sedmice[i].getElementsByClassName("dani"); 
            for(j=0; j<7; j++){
            //za celije na kalendaru prije posljednjeg dana u mjesecu
                if (daniBrojac<= zadnjiDan){
                    if(i=== 1 && j<prviDan){
                    //da se ne prikacuje div sa oznakom zauzeta ili slobodna sala
                    daniZauzece[j].style.display = "none";
                    //da se ne  prikazuje border u li elementu 
                    daniZauzeceLi[j].style.border ="none";
                     //da se broj koji je ostao od proslog mjeseca ponisti 
                     dan[j].innerHTML = '';
                    //da se ne prikazuje div sa brojem dana u mjesecu
                    dan[j].style.display = "none";
                    }
                //za ostale dane na kalendru
                    else{
                    daniZauzece[j].style.display = "inline-block";
                    daniZauzeceLi[j].style.border ="2px solid darkblue";
                    dan[j].style.display = "block";
                    daniZauzece[j].style.backgroundColor = "#86abf9";
                    dan[j].innerHTML = daniBrojac;
                    daniBrojac++;
                }
                }
            //za celije u kalendaru poslije zadnjeg dana u mjesecu
            else{
                //da se ne prikacuje div sa oznakom zauzeta ili slobodna sala
                daniZauzece[j].style.display = "none";
                //da se ne  prikazuje border u li elementu 
                daniZauzeceLi[j].style.border ="none";
                //da se broj koji je ostao od proslog mjeseca ponisti 
                dan[j].innerHTML = '';
                //da se ne prikazuje div sa brojem dana u mjesecu
                dan[j].style.display = "none";
            }
        }            

    }   
} 
function provjeriDaLiJeZauzetoImpl(danUSedmici, semestar, pocetak, kraj, godina, sala, dan, mjesec, osoba){
    var trenutniDanUSedmici;
    for(i=0 ; i<vanrednaZauzeca.length; i++){
        datum = vanrednaZauzeca[i].datum;
        danUMjesecu = datum[0] + datum[1];
        mjesecUGodini = datum[3] + datum[4];
        var godinaZaDatum = datum[6] + datum[7] + datum[8] + datum[9];
        var datumZaDan = new Date(Number(godinaZaDatum), Number(mjesecUGodini)-1, Number(danUMjesecu));
        var trenutniDanUSedmici = datumZaDan.getDay();
        if(trenutniDanUSedmici===0)
            trenutniDanUSedmici = 6;
        else    trenutniDanUSedmici--;
        if((((Number(mjesecUGodini)-1)>=9 || (Number(mjesecUGodini)-1)===0) && semestar ==="zimski") || ((Number(mjesecUGodini)-1)>=1 && (Number(mjesecUGodini)-1)<=5 && semestar==="ljetni")){
            if(danUSedmici === trenutniDanUSedmici && ((vanrednaZauzeca[i].pocetak <= pocetak && pocetak<= vanrednaZauzeca[i].kraj)||( vanrednaZauzeca[i].pocetak<=kraj &&  kraj<=vanrednaZauzeca[i].kraj) ||(vanrednaZauzeca[i].pocetak>=pocetak && vanrednaZauzeca[i].kraj<= kraj)) && vanrednaZauzeca[i].sala === sala){
                //alert("Nije moguće rezervisati salu "+sala + " za navedeni datum " + dan + "/" + mjesec + "/" + godina + " i termin od " + pocetak + " do " + kraj +"!");
                alert("Postoji vanredno  zauzece, pa se ne može zauzeti ovaj termin!");
                console.log(vanrednaZauzeca[i])
                return false;
            }
        }
        else{
           // alert("Nije moguće rezervisati salu "+sala + " za navedeni datum " + dan + "/" + mjesec + "/" + godina + " i termin od " + pocetak + " do " + kraj +"!");
        }

    }
    return true;
}
    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl,
        provjeriDaLiJeZauzeto: provjeriDaLiJeZauzetoImpl
        
    
    }
}());


    
