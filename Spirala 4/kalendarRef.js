window.onload = function(){
   // Kalendar.iscrtajKalendar(this.kalendarDiv, this.tekuciMjesec) ;
    //redovnaZauzeca = Pretvaranje.unesiRedovnaZauzeca();
    //vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
   // this.ucitajZauzecaRef(this.redovnaZauzeca, this.vanrednaZauzeca);
}

var tekuciMjesec = new Date().getMonth();
var kalendarDiv  = document.getElementsByClassName("velikiKalendar"); 
var mjesec;
var mjesecKaoBroj;
var mjesecKaoString;
var datum = new Date();
var godina = datum.getFullYear();
var sedmice;
var dan;
var daniZauzece;
var daniZauzeceLi;
var redovnaZauzeca;
var vanrednaZauzeca;
var salaElement;
var sala;
var pocetak;
var kraj;
var slobodni;
var i;


//kad se zauzme neki dan promijeni se klasa elementa na zauzet
    //kalendarRef, mjesec, sala, vrijemePocetka, vrijemeKraja
function obojiZauzecaRef (){
    
     mjesec = document.getElementById("mjesec").textContent;
     //pretvara mjesec iz stringa u broj
     mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);

    salaElement = document.getElementById("selectSale");
     //uzimanje vrijednosti koja je odabrana u select-u

     sala = salaElement.options[salaElement.selectedIndex].value;

     pocetak = document.getElementById("pocetak").value;
     kraj = document.getElementById("kraj").value;

     Kalendar.obojiZauzeca(kalendarDiv, mjesecKaoBroj, sala, pocetak, kraj);
     
    
}
function ucitajZauzecaRef(){
    Kalendar.ucitajPodatke(redovnaZauzeca,vanrednaZauzeca);
}

function funkcijaIscrtajNazad(){
    //brisanje svih zauzeca na mjesecu sa kojeg prelazimo
    sedmice= kalendarDiv[0].getElementsByTagName("ul");
    for(i=1; i<7; i++){
       //div element koji oznacava da li je zauzeta sala ili ne
       daniZauzece = sedmice[i].getElementsByClassName("zauzeti");
       for(j=0; j<daniZauzece.length; j++){
            daniZauzece[j].backgroundColor = "";
       }
    }

    mjesec = document.getElementById("mjesec").textContent;
    mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);

    if(mjesecKaoBroj===0){

    }
    else{
        
        mjesecKaoBroj--;
        Kalendar.iscrtajKalendar(kalendarDiv,mjesecKaoBroj);
        mjesecKaoString = Pretvaranje.pretvoriMjesecUString(mjesecKaoBroj);
        document.getElementById("mjesec").innerHTML = mjesecKaoString;
    }
    obojiZauzecaRef();
}

function funkcijaIscrtajNaprijed(){
    //brisanje svih zauzeca na mjesecu sa kojeg prelazimo
    sedmice= kalendarDiv[0].getElementsByTagName("ul");
    for(i=1; i<7; i++){
       //div element koji oznacava da li je zauzeta sala ili ne
       daniZauzece = sedmice[i].getElementsByClassName("zauzeti");
       for(j=0; j<daniZauzece.length; j++){
            daniZauzece[j].backgroundColor = "";
       }
    }

    mjesec = document.getElementById("mjesec").textContent;
    mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    if(mjesecKaoBroj===11){
   
    }
    else{
        mjesecKaoBroj++;
        Kalendar.iscrtajKalendar(kalendarDiv,mjesecKaoBroj);
        mjesecKaoString = Pretvaranje.pretvoriMjesecUString(mjesecKaoBroj);
        document.getElementById("mjesec").innerHTML =  mjesecKaoString; 
    } 
    obojiZauzecaRef();

}

 



