

let Poziv = (function(){
    var objekat;
    var datum;
    var godina;
    var datumZauzeca;
    var jsonZauzeca;
    var ajax;
    var lijevo = 0;
    var desno = 2;
    var br;
    var konekcija;
    //Zadatak1 Spirala 4
    function ucitajProfesoreImpl (){
        
        ajax = new XMLHttpRequest();
        ajax.overrideMimeType("application/json");
        ajax.open('GET', 'http://localhost:8080/osoblje', true);
        ajax.onreadystatechange = function (){
            console.log(ajax.readyState + ' '+ ajax.status);
            if(ajax.readyState == 4 && ajax.status == "200"){
                
                var osobe = JSON.parse(this.responseText);
                var selectOsoblje = document.getElementById("predavaci");
                for(br=0; br<osobe.length; br++){
                    
                    var opt = document.createElement("OPTION");
                    //x.setAtribute("value", osobe[br].ime+ ' '+ osobe[br].prezime+ ' '+ osobe[br].uloga);
                    var txt = document.createTextNode(osobe[br].ime+ ' '+ osobe[br].prezime+ ' '+ osobe[br].uloga);
                    opt.appendChild(txt);
                    selectOsoblje.appendChild(opt);
                }
            }
        }
        ajax.send();
    }
    //zadatak2 spirala4
    function ucitajZauzecaBazaImpl(){
        konekcija = new XMLHttpRequest();
        konekcija.overrideMimeType("application/json");
        konekcija.open('GET', 'http://localhost:8080/zauzeca', true);
        konekcija.onreadystatechange = function (){
            if(konekcija.readyState == 4 && konekcija.status == "200"){
                var zauzecaBaza = JSON.parse(this.responseText);
                Kalendar.ucitajPodatke(zauzecaBaza.periodicna, zauzecaBaza.vanredna);
                obojiZauzecaRef();
                console.log("oboji ponovo");
                //alert(zauzecaBaza.periodicna);
            }
        }
        konekcija.send();
    }
    function upisiUBazuImpl(sala, pocetak, kraj, dan, mjesec, periodicna, ime, prezime, uloga){
        datum = new Date();
        godina = datum.getFullYear();

        var conf = confirm(sala + ' ' + pocetak + ' ' + kraj + ' ' + dan + ' ' + mjesec + ' ' + periodicna);
        //ako je potvrđen unos zauzeca
        if (conf == true){
            //ako je zauzece periodicno
            if(periodicna === true){
                var ajax1 = new XMLHttpRequest();
                ajax1.open('POST', 'http://localhost:8080/unesiZauzecaPeriodicna', true);
                ajax1.setRequestHeader("Content-Type", "application/json");
                var kreirajDatum = new Date (godina, mjesec, dan);
                var danUSedmici = kreirajDatum.getDay();
                var semestar;
                if((mjesec>=9 && mjesec<=11) || mjesec === 0)
                    semestar = "zimski";
                if(mjesec>=1 && mjesec<=5)
                    semestar = "ljetni";

                if(danUSedmici === 0)
                    danUSedmici = 6;
                else danUSedmici--;
                if(Kalendar.provjeriDaLiJeZauzeto(danUSedmici, semestar, pocetak, kraj, godina, sala, dan, mjesec, ime +' ' + prezime + ' ' + uloga)){
                    //sala, pocetak, kraj, dan, mjesec, periodicna, ime, prezime, uloga
                    jsonZauzeca = JSON.stringify({"dan": danUSedmici, "semestar": semestar, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "ime": ime, "prezime": prezime, "uloga": uloga, "periodicna": periodicna });
                    
                    ajax1.send(jsonZauzeca);

                    this.ucitajZauzecaBaza();
                    
                    
                }
                
            }else{  //ako je zauzece vanredno

                if(dan > 0 && dan<=9){
                    dan = '0' + dan;
                }                   
                mjesec = mjesec + 1;
                if (mjesec<=9)
                    mjesec = '0' + mjesec;
          
             
                datumZauzeca = dan + '.' + mjesec + '.' + (new Date).getFullYear();
                var ajax1 = new XMLHttpRequest();
                ajax1.open('POST', 'http://localhost:8080/unesiZauzeca', true);
                ajax1.setRequestHeader("Content-Type", "application/json");
                jsonZauzeca = JSON.stringify({"datum": datumZauzeca, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "ime": ime, "prezime": prezime, "uloga": uloga, "periodicna": periodicna });
                ajax1.send(jsonZauzeca);
                //this.test();
                this.ucitajZauzecaBaza();
                
            }
        }else{

        }

    }
    //Zadatak 3 spirala 4
    function osobljePoSalamaImpl(){
        var osobeAjax = new XMLHttpRequest();
        osobeAjax.overrideMimeType("application/json");
        osobeAjax.open('GET', 'http://localhost:8080/osobe', true);
        osobeAjax.onreadystatechange = function (){
            if(osobeAjax.readyState == 4 && osobeAjax.status == "200"){
               // console.log(this.responseText);
                //alert(this.responseText);
               // var tabela = document.getElementsByClassName("tabelaSala")[0];
                var podaci = JSON.parse(this.responseText);
                var brojReda = 1;
                /*var redovi = tabela.getElementsByTagName("tr");
               
                var redZaBrisanje;
                var brojRedova = redovi.length;*/
                //for(var n= 0; n<brojRedova; n++)
                /*var sadrzajDiv = document.getElementsByClassName('sadrzaj')[0];
                
                var tabela = document.createElement("table");
                tabela.className = 'tabelaSala';
                sadrzajDiv.appendChild(tabela);*/
                var tabela = document.getElementsByClassName("tabelaSala")[0];
                 var redovi = tabela.getElementsByTagName("tr");
                var brojRedova = redovi.length;
                for(var n= 0; n<brojRedova; n++){
                    if(redovi.length!=0){
                        tabela.deleteRow(0);
                    }
                }

               // var tblBody = document.createElement("tbody");
               var red = tabela.insertRow(0);
               var celija1 = red.insertCell(0);
               var celija2 = red.insertCell(1);
               celija1.className = 'headerSprat';
               celija2.className = 'headerSprat';
               celija1.innerHTML = "Ime i prezime";
               celija2.innerHTML = "Sala";
                for(var counter= 0; counter<podaci.sviProfesori.length; counter++){
                    for(var counter1=0 ; counter1<podaci.zauzetiProfesori.length; counter1++){
                        if(podaci.sviProfesori[counter] == podaci.zauzetiProfesori[counter1].ime){

                            red = tabela.insertRow(brojReda);
                            brojReda++;
                            celija1 = red.insertCell(0);
                            celija2 = red.insertCell(1);
                            celija1.className = 'sala';
                            celija2.className = 'sala';
                            celija1.innerHTML = podaci.zauzetiProfesori[counter1].ime;
                            celija2.innerHTML = podaci.zauzetiProfesori[counter1].sala;
                            continue;
                        }
                       
                    }
                }

            }
        }
        osobeAjax.send();
    }
    //Spirala 3
    function testImpl (){
        ajax = new XMLHttpRequest();
        ajax.overrideMimeType("application/json");
        ajax.open('GET', 'http://localhost:8080/zauzeca', true);
        ajax.onreadystatechange = function (){
            if(ajax.readyState == 4 && ajax.status == "200"){
                //alert(this.responseText);
                objekat = JSON.parse(this.responseText);
                Kalendar.ucitajPodatke(objekat.periodicna, objekat.vanredna);
                obojiZauzecaRef();

            }
        }
        ajax.send();
    }
    function klikZaRezervacijuImpl(sala, pocetak, kraj, dan, mjesec, periodicna){
       /* datum = new Date();
        godina = datum.getFullYear();

        var conf = confirm(sala + ' ' + pocetak + ' ' + kraj + ' ' + dan + ' ' + mjesec + ' ' + periodicna);*/
        //ako je potvrđen unos zauzeca
       // if (conf == true){
            //ako je zauzece periodicno
           /* if(periodicna === true){
                var ajax1 = new XMLHttpRequest();
                ajax1.open('POST', 'http://localhost:8080/unesiZauzecaPeriodicna', true);
                ajax1.setRequestHeader("Content-Type", "application/json");
                var kreirajDatum = new Date (godina, mjesec, dan);
                var danUSedmici = kreirajDatum.getDay();
                var semestar;
                if((mjesec>=9 && mjesec<=11) || mjesec === 0)
                    semestar = "zimski";
                if(mjesec>=1 && mjesec<=5)
                    semestar = "ljetni";

                if(danUSedmici === 0)
                    danUSedmici = 6;
                else danUSedmici--;
                if(Kalendar.provjeriDaLiJeZauzeto(danUSedmici, semestar, pocetak, kraj, godina, sala, dan, mjesec)){
                    jsonZauzeca = JSON.stringify({"dan": danUSedmici, "semestar": semestar, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "predavac": ''});
                    ajax1.send(jsonZauzeca);
                    this.test();
                }
                
            }else{*/  //ako je zauzece vanredno
/*
                if(dan > 0 && dan<=9){
                    dan = '0' + dan;
                }                   
                mjesec = mjesec + 1;
                if (mjesec<=9)
                    mjesec = '0' + mjesec;*/

               /* var zauzeceTextJSON = '{ "dan": "' + dan + '.' + mjesec + '.' + (new Date).getFullYear() + '.",\n' +
                                       '"pocetak": "' + pocetak + '",\n' + 
                                       '"kraj": "' + kraj + '",\n' + 
                                       '"naziv": ' + sala + '",\n'+
                                       '"predavac": \n' +
                                       '}';    */            
             
               // datumZauzeca = dan + '.' + mjesec + '.' + (new Date).getFullYear() + '.';
                //objekat.vanredna.push({"dan": datumZauzeca, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "predavac": ''});
              /*  var ajax1 = new XMLHttpRequest();
                ajax1.open('POST', 'http://localhost:8080/unesiZauzeca', true);
                ajax1.setRequestHeader("Content-Type", "application/json");
                jsonZauzeca = JSON.stringify({"datum": datumZauzeca, "pocetak": pocetak, "kraj": kraj, "naziv": sala, "predavac": ''});
                ajax1.send(jsonZauzeca);
                this.test();
                
            }
        }else{

        }*/
    }
    function postaviSlikeImpl(div){
        var xhr = new XMLHttpRequest();
        xhr.overrideMimeType("application/json");
        xhr.open('GET', "http://localhost:8080/slike", true);
        xhr.onreadystatechange = function (){
            if(xhr.readyState == 4 && xhr.status == "200"){
               // var objekat = JSON.parse(this.responseText);
               // div.appendChild('<img src='+ objekat[0] + '">');
            }
        }
        xhr.send();
        return true;
    }
    function prikaziSlikeImpl(div){
        var xhr1 = new XMLHttpRequest();
        xhr1.open('GET', "http://localhost:8080/slikeZaPrikaz", true);
        xhr1.setRequestHeader("Content-Type", "application/json");
        xhr1.onreadystatechange = function (){
            if(xhr1.readyState == 4 && xhr1.status == "200"){
                
                 objekat = JSON.parse(this.responseText);
                var slikaElement = document.createElement("img");
                var nizSlika = objekat.slike;
                var xhr2 = new XMLHttpRequest();
                //prva slika
                slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[0].url;
                xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[0].url, true );
                xhr2.send();
                //div.appendChild(slikaElement);
                div.getElementsByClassName("slika")[0].src = "http://localhost:8080/slike/" + objekat.slike[0].url;
                //druga slika
                slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[1].url;
                xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[1].url, true );
                xhr2.send();
                div.getElementsByClassName("slika")[1].src = "http://localhost:8080/slike/" + objekat.slike[1].url;
               // div.appendChild(slikaElement);
                //treca slika
                slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[2].url;
                xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[2].url, true );
                xhr2.send();
                div.getElementsByClassName("slika")[2].src = "http://localhost:8080/slike/" + objekat.slike[2].url;
                //div.appendChild(slikaElement);
            }
        }
        xhr1.send();

    }
    function klikniLijevoImpl(buttonLijevo, buttonDesno, div){
        
        xhr1 = new XMLHttpRequest();
        xhr1.open('GET', "http://localhost:8080/slikeZaPrikaz", true);
        xhr1.setRequestHeader("Content-Type", "application/json");
        xhr1.onreadystatechange = function (){
            if(xhr1.readyState == 4 && xhr1.status == "200"){
                
                    objekat = JSON.parse(this.responseText);
                var slikaElement = document.createElement("img");
                var nizSlika = objekat.slike;
                lijevo--;
               // alert(Number(parseInt(objekat.slike.length/3)));
                if(lijevo === Number(parseInt(objekat.slike.length/3))-1)
                        buttonDesno.disabled = false;
                if(lijevo === 0)
                        buttonLijevo.disabled = true;

                var xhr2 = new XMLHttpRequest();
                //prva slika
                if(lijevo*3 < objekat.slike.length){
                    div.getElementsByClassName("slika")[0].style.visibility = 'visible';
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[0].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3].url;
                }else{
                    div.getElementsByClassName("slika")[0].style.visibility = 'hidden';
                }
               
                //druga slika
                if(lijevo*3+1 < objekat.slike.length){
                    div.getElementsByClassName("slika")[1].style.visibility = 'visible';
                    slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url;
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[1].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url;
                }else{
                    div.getElementsByClassName("slika")[1].style.visibility = 'hidden';
                }
                // div.appendChild(slikaElement);
                //treca slika
                if(lijevo*3+2 < objekat.slike.length){
                    div.getElementsByClassName("slika")[2].style.visibility = 'visible';
                    slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url;
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[2].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url;
                }else{
                    div.getElementsByClassName("slika")[2].style.visibility = 'hidden';
                }
            }
         }
            xhr1.send();
            
    }
    function klikniDesnoImpl(buttonLijevo, buttonDesno, div){
            xhr1 = new XMLHttpRequest();
            xhr1.open('GET', "http://localhost:8080/slikeZaPrikaz", true);
            xhr1.setRequestHeader("Content-Type", "application/json");
            xhr1.onreadystatechange = function (){
            if(xhr1.readyState == 4 && xhr1.status == "200"){
                
                    objekat = JSON.parse(this.responseText);
                var nizSlika = objekat.slike;
                lijevo++;
                if(lijevo === Number(parseInt(objekat.slike.length/3)))
                        buttonDesno.disabled = true;
                if(lijevo === 1)
                    buttonLijevo.disabled = false;
                var xhr2 = new XMLHttpRequest();
                //prva slika
                if(lijevo*3 < objekat.slike.length){
                    div.getElementsByClassName("slika")[0].style.visibility = 'visible';
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[0].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3].url;
                }else{
                    div.getElementsByClassName("slika")[0].style.visibility = 'hidden';
                }
               
                //druga slika
                if(lijevo*3+1 < objekat.slike.length){
                    div.getElementsByClassName("slika")[1].style.visibility = 'visible';
                    slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url;
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[1].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 1].url;
                }else{
                    div.getElementsByClassName("slika")[1].style.visibility = 'hidden';
                }
                // div.appendChild(slikaElement);
                //treca slika
                if(lijevo*3+2 < objekat.slike.length){
                    div.getElementsByClassName("slika")[2].style.visibility = 'visible';
                    slikaElement.src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url;
                    xhr2.open('GET', "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url, true );
                    xhr2.send();
                    div.getElementsByClassName("slika")[2].src = "http://localhost:8080/slike/" + objekat.slike[lijevo*3 + 2].url;
                }else{
                    div.getElementsByClassName("slika")[2].style.visibility = 'hidden';
                }
            }
        }
        xhr1.send();
    }

    return{
       // ucitajJSONZauzeca: ucitajJSONZauzecaImpl,
       // proslijediJSONZauzeca: proslijediJSONZauzecaImpl,
        test: testImpl,
        klikZaRezervaciju: klikZaRezervacijuImpl,
        postaviSlike: postaviSlikeImpl,
        prikaziSlike: prikaziSlikeImpl,
        klikniLijevo: klikniLijevoImpl,
        klikniDesno: klikniDesnoImpl,
        ucitajProfesore: ucitajProfesoreImpl,
        ucitajZauzecaBaza: ucitajZauzecaBazaImpl,
        upisiUBazu: upisiUBazuImpl,
        osobljePoSalama: osobljePoSalamaImpl
    }

}());