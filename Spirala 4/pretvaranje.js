let Pretvaranje = (function (){
    
    function pretvoriMjesecUBrojImpl(mjesecString){
        var mjesecBroj;
        if(mjesecString=== 'Januar'){
            mjesecBroj=0;
        }
        else if(mjesecString === 'Februar'){
            mjesecBroj=1;
        }
        else if(mjesecString === 'Mart'){
            mjesecBroj=2;
        }
        else if(mjesecString === 'April'){
            mjesecBroj=3;
        }
        else if(mjesecString === 'Maj'){
            mjesecBroj=4;
        }
        else if(mjesecString === 'Juni'){
            mjesecBroj=5;
        }
        else if(mjesecString === 'Juli'){
            mjesecBroj=6;
        }
        else if(mjesecString === 'August'){
            mjesecBroj=7;
        }
        else if(mjesecString === 'Septembar'){
            mjesecBroj=8;
        }
        else if(mjesecString === 'Oktobar'){
            mjesecBroj=9;
        }
        else if(mjesecString === 'Novembar'){
            mjesecBroj=10;
        }
        else if(mjesecString === 'Decembar'){
            mjesecBroj=11;
        }
        else;

            
        
        return mjesecBroj;
    }

    function pretvoriMjesecUStringImpl(mjesecBroj){
        var mjesecString;
        if (mjesecBroj === 0){
            mjesecString = 'Januar';
        }
        else if (mjesecBroj === 1){
            mjesecString= 'Februar';
        }
        else if (mjesecBroj === 2){
           mjesecString= 'Mart';
       }
       else if (mjesecBroj === 3){
           mjesecString= 'April';
       }
       else if (mjesecBroj === 4){
           mjesecString= 'Maj';
       }
       else if (mjesecBroj === 5){
           mjesecString= 'Juni';
       }
       else if (mjesecBroj === 6){
           mjesecString= 'Juli';
       }
       else if (mjesecBroj === 7){
           mjesecString= 'August';
       }
       else if (mjesecBroj === 8){
           mjesecString= 'Septembar';
       }
       else if (mjesecBroj === 9){
           mjesecString= 'Oktobar';
       }
       else if (mjesecBroj === 10){
           mjesecString= 'Novembar';
       }
       else if (mjesecBroj === 11){
           mjesecString= 'Decembar';
       }
       else{
   
       }
       return mjesecString;
    }
    function unesiRedovnaZauzecaImpl(){
    
        var redovnaZauzeca = [
    
        {
            dan: 1,
            semestar: "zimski",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "1-02",
            predavac: "Razija Turcinhodzic"
        },
        {
            dan: 1,
            semestar: "zimski",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "0-06",
            predavac: "Anel Tanovic"
        },
        {
            dan: 2,
            semestar: "zimski",
            pocetak: "14:00",
            kraj: "16:00",
            naziv: "1-01",
            predavac: "Vensada Okanovic"
        },
        {
            dan: 3,
            semestar: "zimski",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "1-02",
            predavac: "Selma Rizvic"
        },
        {
            dan: 4,
            semestar: "zimski",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "MA",
            predavac: "Samir Ribic"
        },
        {
            dan: 0,
            semestar: "ljetni",
            pocetak: "15:00",
            kraj: "17:00",
            naziv: "VA1",
            predavac: "Anel Tanovic"
        },
    
        {
            dan: 1,
            semestar: "ljetni",
            pocetak: "12:00",
            kraj: "15:00",
            naziv: "VA2",
            predavac: "Razija Turcinhodzic"
        },
        {
            dan: 1,
            semestar: "ljetni",
            pocetak: "15:00",
            kraj: "17:00",
            naziv: "0-06",
            predavac: "Anel Tanovic"
        },
        {
            dan: 2,
            semestar: "ljetni",
            pocetak: "14:00",
            kraj: "16:00",
            naziv: "0-01",
            predavac: "Vensada Okanovic"
        },
        {
            dan: 3,
            semestar: "ljetni",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "1-02",
            predavac: "Selma Rizvic"
        },
        {
            dan: 4,
            semestar: "ljetni",
            pocetak: "09:00",
            kraj: "12:00",
            naziv: "1-07",
            predavac: "Samir Ribic"
        }
            
        ]
        return redovnaZauzeca;
    }
    function unesiVanrednaZauzecaImpl(){
        var vanrednaZauzeca = [
        {
    
            datum: "04.12.2019",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "EE1",
            predavac: "Razija Turcinhodzic"
                            
        },
        {
        
            datum: "11.11.2019",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "EE1",
            predavac: "Razija Turcinhodzic"
                            
        },
        {
        
            datum: "05.12.2019",
            pocetak: "15:00",
            kraj: "16:30",
            naziv: "MA",
            predavac: "Vensada Okanovic"                
        },
        {
        
            datum: "21.04.2019",
            pocetak: "14:00",
            kraj: "16:00",
            naziv: "0-09",
            predavac: "Ivona Ivkovic"                
        },
        {
        
            datum: "20.11.2019",
            pocetak: "14:00",
            kraj: "16:00",
            naziv: "1-01",
            predavac: "Anel Tanovic"                
        },
        {
        
            datum: "05.12.2019",
            pocetak: "15:00",
            kraj: "16:30",
            naziv: "VA2",
            predavac: "Vensada Okanovic"                
        }
        ];
        return vanrednaZauzeca;
    }
    function zauzetiSviDaniImpl(){
        var redovnaZauzecaSviDani = [
    
            {
                dan: 0,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
            {
                dan: 1,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
            {
                dan: 2,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
            {
                dan: 3,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
            {
                dan: 4,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
            {
                dan: 5,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            },
        
            {
                dan: 6,
                semestar: "zimski",
                pocetak: "09:00",
                kraj: "12:00",
                naziv: "1-02",
                predavac: "Razija Turcinhodzic"
            }
                
            ]
            return redovnaZauzecaSviDani;
    }

    return {
        pretvoriMjesecUBroj: pretvoriMjesecUBrojImpl,
        pretvoriMjesecUString: pretvoriMjesecUStringImpl,
        unesiRedovnaZauzeca: unesiRedovnaZauzecaImpl,
        unesiVanrednaZauzeca: unesiVanrednaZauzecaImpl,
        zauzetiSviDani : zauzetiSviDaniImpl
    }

    
}());