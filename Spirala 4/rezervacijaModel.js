const Sequelize = require("sequelize");

module.exports = function(sequelize, DataTypes){
    const Rezervacija = sequelize.define("rezervacija",{
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        terminId: {
            type: Sequelize.INTEGER,
            references: {
                model:'termins', 
                key: 'id',
                unique: true
            }
        },
        salaId : {
            type: Sequelize.INTEGER,
            references:{
                model: 'salas',
                key: 'id'
            } 
        }
       /* osobaId: {
            type: Sequelize.INTEGER,
            references:{
                model: 'osobljes',
                key: 'id'
            } 
        }*/

    })
    return Rezervacija;
};