//Poziv.ucitajJSONZauzeca();
var tekuciMjesec = new Date().getMonth();
var kalendarDiv  = document.getElementsByClassName("velikiKalendar"); 
var sadrzaj = document.getElementsByClassName("sadrzaj");
var mjesec;
var mjesecKaoBroj;
var danUMjesecu;
var salaElement;
var sala;
var pocetak;
var kraj;
var periodicna;
var periodicnaBool;
var predavaciSelect;
var predavaci;
function ucitaj(){
    //Ispisivanje trenutnog mjeseca
    mjesec = Pretvaranje.pretvoriMjesecUString(tekuciMjesec);
    console.log(mjesec);
    document.getElementById("mjesec").innerHTML = mjesec;
    Kalendar.iscrtajKalendar(kalendarDiv, tekuciMjesec);
    Poziv.ucitajProfesore();
    //Ucitavanje podataka
  //  Poziv.test();
    Poziv.ucitajZauzecaBaza();
    

}
function confirmProzor(div){ 
    //Uzima dan na kalendaru na koji je kliknuto
    danUMjesecu = div.firstElementChild.innerHTML;
    //Mjesec u kojem zelimo zauzece
    mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(document.getElementById("mjesec").innerHTML);
    //Sala koju zelimo zauzeti
    salaElement = document.getElementById("selectSale");
    sala = salaElement.options[salaElement.selectedIndex].value;
    //Vrijeme pocetka 
    pocetak = document.getElementById("pocetak").value;
    //Vrijeme kraja
    kraj = document.getElementById("kraj").value;
    //Da li je preriodicna
    periodicna = document.getElementById("periodicna");
    
    periodicnaBool = periodicna.checked;

    predavaciSelect = document.getElementById("predavaci");
    predavaci = predavaciSelect.options[predavaciSelect.selectedIndex].value;
    var predavaciParam;
    predavaciParam = predavaci.split(" ");

    //Izbacuje confirm prozor za dane dane na kalendaru koji su slobodni
    if(div.getElementsByClassName("slobodna")[0].style.backgroundColor == "green")
        //sala, pocetak, kraj, dan, mjesec, periodicna
       // Poziv.klikZaRezervaciju(sala, pocetak, kraj, danUMjesecu, mjesecKaoBroj, periodicnaBool);
       Poziv.upisiUBazu(sala, pocetak, kraj, danUMjesecu, mjesecKaoBroj, periodicnaBool, predavaciParam[0], predavaciParam[1], predavaciParam[2]);
    else{
        mjesecKaoBroj++;
        if(mjesecKaoBroj<=9)
            mjesecKaoBroj = '0' + mjesecKaoBroj;
        var datum = new Date();
        var godina = datum.getFullYear();
        alert("Nije moguće rezervisati salu "+sala + " za navedeni datum " + danUMjesecu + "/" + mjesecKaoBroj + "/" + godina + " i termin od " + pocetak + " do " + kraj +"!");
    } 
    
}
/*
setInterval(function(){
    obojiZauzecaRef();
},3000);*/