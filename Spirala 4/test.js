var assert = chai.assert;
describe('rezervacije', () => {
 describe('obojiZauzeca()', () => {
   //Pozivanje obojiZauzeca kada podaci nisu učitani: očekivana vrijednost da se ne oboji niti jedan dan
   it('Pozivanje obojiZauzeca kada podaci nisu učitani', () => {
    
    var kalendarDiv = document.getElementsByClassName("velikiKalendar");
    var mjesec = document.getElementById("mjesec").textContent;
    var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    var salaElement = document.getElementById("selectSale");
    var sala = salaElement.options[salaElement.selectedIndex].value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;

    Kalendar.iscrtajKalendar(kalendarDiv, mjesecKaoBroj);
    Kalendar.obojiZauzeca(kalendarDiv, mjesecKaoBroj, sala, pocetak, kraj);

    var counter = 0;
    var slobodni = document.getElementsByClassName("slobodna"); 
    
    for(var i = 0; i<slobodni.length; i++){
        if(slobodni[i].style.backgroundColor== "red"){
            counter++;
        }
       
    }
     assert.equal(counter, 0,"Error!");
   })
   //Pozivanje obojiZauzeca gdje u zauzećima postoje duple vrijednosti za zauzeće istog termina: očekivano je da se dan oboji bez obzira što postoje duple vrijednosti

   it('Pozivanje obojiZauzeca gdje u zauzećima postoje duple vrijednosti za zauzeće istog termina', () => {

    //testira za srijedu i 20.11.

    var kalendarDiv = document.getElementsByClassName("velikiKalendar");
    var mjesec = document.getElementById("mjesec").textContent;
    var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    var salaElement = document.getElementById("selectSale");
    var sala = salaElement.options[salaElement.selectedIndex].value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var redovnaZauzeca = Pretvaranje.unesiRedovnaZauzeca();
    var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
    
    //Uzima 20.11.2019. kada u isto vrijeme imaju dva zauzeca za istu salu
    Kalendar.iscrtajKalendar(kalendarDiv, 10);
    Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
    Kalendar.obojiZauzeca(kalendarDiv, 10, "1-01", "14:00", "16:00");
    
   
    var slobodni = document.getElementsByClassName("slobodna");
    var dani = document.getElementsByClassName("broj"); 
    var counter = 0;
    for(var i = 0; i<dani.length; i++){
        if(dani[i].innerHTML === "20" && slobodni[i].style.backgroundColor === "red"){
            counter++;
        }
       
    }
     assert.equal(counter, 1,"Error!");
   })
   //Pozivanje obojiZauzece kada u podacima postoji periodično zauzeće za drugi semestar: očekivano je da se ne oboji zauzeće

   it('Pozivanje obojiZauzece kada u podacima postoji periodično zauzeće za drugi semestar', () => {
    /* {
            dan: 1,
            semestar: "ljetni",
            pocetak: "15:00",
            kraj: "17:00",
            naziv: "0-06",
            predavac: "Anel Tanovic"
        }*/
    var kalendarDiv = document.getElementsByClassName("velikiKalendar");
    var mjesec = document.getElementById("mjesec").textContent;
    var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    var salaElement = document.getElementById("selectSale");
    var sala = salaElement.options[salaElement.selectedIndex].value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var redovnaZauzeca = Pretvaranje.unesiRedovnaZauzeca();
    var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
    
    Kalendar.iscrtajKalendar(kalendarDiv, 10);
    Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
    Kalendar.obojiZauzeca(kalendarDiv, 10, "0-06", "15:00", "17:00");
    
   
    var slobodni = document.getElementsByClassName("slobodna");
    var dani = document.getElementsByClassName("broj"); 
    var counter = 0;
    for(var i = 0; i<dani.length; i++){
        if((dani[i].innerHTML === "5" || dani[i].innerHTML === "12" || dani[i].innerHTML === "19" || dani[i].innerHTML === "26") && slobodni[i].style.backgroundColor === "red"){
            counter++;
        }
       
    }
     assert.equal(counter, 0,"Error!");
   })
   //Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu: očekivano je da se ne oboji zauzeće
   it('Pozivanje obojiZauzece kada u podacima postoji zauzeće termina ali u drugom mjesecu', () =>{
    /* {
            datum: "04.12.2019",
            pocetak: "11:00",
            kraj: "12:00",
            naziv: "EE1",
            predavac: "Razija Turcinhodzic"
                            
        }*/
    var kalendarDiv = document.getElementsByClassName("velikiKalendar");
    var mjesec = document.getElementById("mjesec").textContent;
    var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    var salaElement = document.getElementById("selectSale");
    var sala = salaElement.options[salaElement.selectedIndex].value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var redovnaZauzeca = Pretvaranje.unesiRedovnaZauzeca();
    var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
    
    Kalendar.iscrtajKalendar(kalendarDiv, 10);
    Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
    Kalendar.obojiZauzeca(kalendarDiv, 10, "EE1", "11:00", "12:00");
    
   
    var slobodni = document.getElementsByClassName("slobodna");
    var dani = document.getElementsByClassName("broj"); 
    var counter = 0;
    for(var i = 0; i<dani.length; i++){
        if(dani[i].innerHTML === "4" && slobodni[i].style.backgroundColor === "red"){
            counter++;
        }
       
    }
     assert.equal(counter, 0,"Error!");
   })
   //Pozivanje obojiZauzece kada su u podacima svi termini u mjesecu zauzeti: očekivano je da se svi dani oboje
   
  it('Pozivanje obojiZauzece kada su u podacima svi termini u mjesecu zauzeti', () => {
    /* {
        SVAKI DAN
        semestar: "zimski",
        pocetak: "09:00",
        kraj: "12:00",
        naziv: "1-02",
        predavac: "Razija Turcinhodzic"
                            
        }*/
    var kalendarDiv = document.getElementsByClassName("velikiKalendar");
    var mjesec = document.getElementById("mjesec").textContent;
    var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
    var salaElement = document.getElementById("selectSale");
    var sala = salaElement.options[salaElement.selectedIndex].value;
    var pocetak = document.getElementById("pocetak").value;
    var kraj = document.getElementById("kraj").value;
    var redovnaZauzeca = Pretvaranje.zauzetiSviDani();
    var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
    
    Kalendar.iscrtajKalendar(kalendarDiv, 10);
    Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
    Kalendar.obojiZauzeca(kalendarDiv, 10, "1-02", "09:00", "12:00");
    
    
    var slobodni = document.getElementsByClassName("slobodna");
    var dani = document.getElementsByClassName("broj"); 
    var counter = 0;
    for(var i = 0; i<dani.length; i++){
        if(slobodni[i].style.backgroundColor === "red"){
            counter++;
        }
        
    }
      assert.equal(counter, 30,"Error!");
    })
    //Dva puta uzastopno pozivanje obojiZauzece: očekivano je da boja zauzeća ostane ista
    it('Dva puta uzastopno pozivanje obojiZauzece', () => {
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
      var salaElement = document.getElementById("selectSale");
      var sala = salaElement.options[salaElement.selectedIndex].value;
      var pocetak = document.getElementById("pocetak").value;
      var kraj = document.getElementById("kraj").value;
      var redovnaZauzeca = Pretvaranje.zauzetiSviDani();
      var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
      
      Kalendar.iscrtajKalendar(kalendarDiv, 10);
      Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
      Kalendar.obojiZauzeca(kalendarDiv, 10, "1-02", "09:00", "12:00");
      
      
      var slobodni = document.getElementsByClassName("slobodna");
      var dani = document.getElementsByClassName("broj"); 
      
      var counter = 0;
      for(var i = 0; i<dani.length; i++){
          if(slobodni[i].style.backgroundColor === "red"){
              counter++;
          }
          
      }
      Kalendar.obojiZauzeca(kalendarDiv, 10, "1-02", "09:00", "12:00");
      slobodni = document.getElementsByClassName("slobodna");
      dani = document.getElementsByClassName("broj"); 
      var counter1 = 0;
      for(var i = 0; i<slobodni.length; i++){
        if(slobodni[i].style.backgroundColor === "red"){
            counter1++;
        }
        
    }
        assert.equal(counter, counter1,"Error!");
      })
      //Pozivanje ucitajPodatke, obojiZauzeca, ucitajPodatke - drugi podaci, obojiZauzeca: očekivano da se zauzeća iz prvih podataka ne ostanu obojena, tj. primjenjuju se samo posljednje učitani podaci
    it('Pozivanje ucitajPodatke, obojiZauzeca, ucitajPodatke - drugi podaci, obojiZauzeca', () => {
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      var mjesecKaoBroj = Pretvaranje.pretvoriMjesecUBroj(mjesec);
      var salaElement = document.getElementById("selectSale");
      var sala = salaElement.options[salaElement.selectedIndex].value;
      var pocetak = document.getElementById("pocetak").value;
      var kraj = document.getElementById("kraj").value;

      var redovnaZauzeca = Pretvaranje.zauzetiSviDani();
      var vanrednaZauzeca = Pretvaranje.unesiVanrednaZauzeca();
      Kalendar.iscrtajKalendar(kalendarDiv, 10);
      Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
      Kalendar.obojiZauzeca(kalendarDiv, 10, "1-02", "09:00", "12:00");
      
      var slobodni = document.getElementsByClassName("slobodna");
      var dani = document.getElementsByClassName("broj"); 
      
      var counter = 0;
      for(var i = 0; i<dani.length; i++){
          if(slobodni[i].style.backgroundColor === "red"){
              counter++;
          }
          
      }
      redovnaZauzeca = Pretvaranje.unesiRedovnaZauzeca();
      Kalendar.ucitajPodatke(redovnaZauzeca, vanrednaZauzeca);
      Kalendar.obojiZauzeca(kalendarDiv, 10, "1-02", "09:00", "12:00");
      slobodni = document.getElementsByClassName("slobodna");
      dani = document.getElementsByClassName("broj"); 
      var counter1 = 0;
      for(var i = 0; i<slobodni.length; i++){
        if(slobodni[i].style.backgroundColor === "red"){
            counter1++;
        }
        
    }
        assert.equal(counter1, 8,"Error!");
      })
  })
  describe('iscrtajKalendar()', () => {
    //Pozivanje iscrtajKalendar za mjesec sa 30 dana: očekivano je da se prikaže 30 dana
    it('Pozivanje iscrtajKalendar za mjesec sa 30 dana', () =>{
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      //za novembar
      Kalendar.iscrtajKalendar(kalendarDiv,10);
      var dani = document.getElementsByClassName("broj");
      var counter = 0;
      for(var i = 0; i<dani.length; i++){
        if(dani[i].innerHTML != ''){
          counter++;
        }
      }
      
        assert.equal(counter, 30,"Error!");
    });
    //Pozivanje iscrtajKalendar za mjesec sa 31 dan: očekivano je da se prikaže 31 dan
    it('Pozivanje iscrtajKalendar za mjesec sa 31 dana', () =>{
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      //za decembar
      Kalendar.iscrtajKalendar(kalendarDiv,11);
      var dani = document.getElementsByClassName("broj");
      var counter = 0;
      for(var i = 0; i<dani.length; i++){
        if(dani[i].innerHTML != ''){
          counter++;
        }
      }
      
        assert.equal(counter, 31,"Error!");
    });
     //Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 1. dan u petak
     it('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 1. dan u petak', () =>{
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      //za novembar
      Kalendar.iscrtajKalendar(kalendarDiv,10);
      var sedmice = kalendarDiv[0].getElementsByTagName("ul");
     
      var prviDan;
      for(var i = 1; i<sedmice.length; i++){
        var dani = sedmice[i].getElementsByClassName("broj");
        for(var j=0; j<dani.length; j++){
          if(dani[j].innerHTML === "1"){
            prviDan = j;
         }   
        }
         
      }
        
        assert.equal(prviDan, 4,"Error!");
    });
    //Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 30. dan u subotu
    it('Pozivanje iscrtajKalendar za trenutni mjesec: očekivano je da je 30. dan u subotu', () =>{
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      //za novembar
      Kalendar.iscrtajKalendar(kalendarDiv,10);
      var sedmice = kalendarDiv[0].getElementsByTagName("ul");
     
      var prviDan;
      for(var i = 1; i<sedmice.length; i++){
        var dani = sedmice[i].getElementsByClassName("broj");
        for(var j=0; j<dani.length; j++){
          if(dani[j].innerHTML === "30"){
            prviDan = j;
         }   
        }
         
      }
        
        assert.equal(prviDan, 5,"Error!");
    });
    //Pozivanje iscrtajKalendar za januar: očekivano je da brojevi dana idu od 1 do 31 počevši od utorka
    it('Pozivanje iscrtajKalendar za januar: očekivano je da brojevi dana idu od 1 do 31 počevši od utorka', () =>{
      var prvi;
      var zadnji;
      var kalendarDiv = document.getElementsByClassName("velikiKalendar");
      var mjesec = document.getElementById("mjesec").textContent;
      //za novembar
      Kalendar.iscrtajKalendar(kalendarDiv,0);
      var sedmice = kalendarDiv[0].getElementsByTagName("ul");
    
      var prviDan;
      for(var i = 1; i<sedmice.length; i++){
        var dani = sedmice[i].getElementsByClassName("broj");
        for(var j=0; j<dani.length-1; j++){
          if(dani[j].innerHTML === "1"){
            prviDan = j;
          }
          if(dani[j].innerHTML === '' && dani[j+1].innerHTML !== ''){
            prvi = dani[j+1].innerHTML;
           
          } 
           if(dani[j].innerHTML !== '' && dani[j+1].innerHTML === ''){
            zadnji = dani[j].innerHTML;
            console.log(dani[j].innerHTML);
          }
        }
        
      }
      
      assert.equal(prviDan, 1,"Error!");
      assert.equal(prvi, '1',"Error!");
      assert.equal(zadnji, '31',"Error!");
    });
  });
});
